package com.rakuten.training.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.rakuten.training.dal.ProductDAO;

@Service
public class SpecialUseCaseBean {
	
	ProductDAO dao;
	
	@Autowired
	@Qualifier("productDAOInMemImpl")
	public void setDao(ProductDAO dao) {
		System.out.println("Special use case bean got injected with an object of: "+dao.getClass().getName());
		this.dao = dao;
	}

}

package com.rakuten.training;

import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.rakuten.training.dal.ProductRepository;
import com.rakuten.training.domain.Product;

@SpringBootApplication
public class ProductsApplication {


	public static void main(String[] args) {
		
//		ApplicationContext springContainer = 
				SpringApplication.run(ProductsApplication.class, args);
//		ProductConsoleUI ui = springContainer.getBean(ProductConsoleUI.class);
//		ui.createProductWithUI();
		
//		testRepository(springContainer);
	}

//	private static void testRepository(ApplicationContext springContainer) {
//		ProductRepository repo = springContainer.getBean(ProductRepository.class);
//		Optional<Product> op = repo.findById(1);
//		if(op.isPresent()) {
//			System.out.println("Product price = "+op.get().getPrice());
//		}else {
//			System.out.println("Product not found!!");
//		}
//	}

}

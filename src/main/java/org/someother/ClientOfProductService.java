package org.someother;

import org.springframework.web.client.RestTemplate;

import com.rakuten.training.domain.Product;

public class ClientOfProductService {

	static final String BASE_URL = "http://localhost:8080";
	
	public static void main(String[] args) {
		int id = 1;
		RestTemplate template = new RestTemplate();
		Product result = template.getForObject(BASE_URL+"/products/{id}",Product.class, id);
		System.out.println(result);

	}

}
